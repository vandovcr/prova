echo "    ############################################################################
    #############               SCRIPT POR                   ###################
    #############               Apavanello                   ###################
    #############               VER 0.5                      ###################
    ############################################################################"


if [ `whoami` != root ]; then
    echo "

    ############################################################################
    ############          Para que esse script rode com sucesso         ########  
    ############          por favor execute como root ou usando SUDO    ########
    ############################################################################"
    exit
fi

echo "nameserver 8.8.8.8" > /etc/resolv.conf

echo "

Iniciando a instalacao de recursos obrigatorios para a finalização com sucesso deste script
LOGS ~/log/yum-update.log e ~/log/yum-install-phase1.log"
mkdir -p ~/log
mkdir -p /tmp/chaves/
yum update -y > ~/log/yum-update.log
yum install vim wget git curl -y > ~/log/yum-install-phase1.log

echo "

Iniciando a instalação de recursos utilizados para a prova 
LOGS ~/log/yum-install-phase2.log e ~/log/yum-install-phase3.log"

yum install epel-release google-authenticator -y > ~/log/yum-install-phase2.log
yum install mailx -y > ~/log/yum-install-phase3.log
adduser apavanello 
echo "123123" | passwd apavanello --stdin

echo "
Preparando e Blindando o SSH

Aplicando blindagem PAM e AUTHENTICATOR
"

sed -i 's/ChallengeResponseAuthentication no.*/ChallengeResponseAuthentication yes/' /etc/ssh/sshd_config 2> ~/log/ssh_sed
echo "auth required pam_google_authenticator.so nullok" >> /etc/pam.d/sshd
echo "AllowUsers root apavanello" >> /etc/ssh/sshd_config

systemctl restart sshd 2> ~/log/ssh_reboot

echo "

Iniciando a criacao de usuarios:
Quantos usuarios serão criados? [Digite 0 para sair do script]"

read qnt_user

re='^[0-9]+$'
if ! [[ $qnt_user =~ $re ]] ; then
   echo "
   Entrada invalida (nao numerica), abortando o script" >&2; exit 1
fi

for (( a=1 ; a <= $qnt_user ; a++))
do
    exit="False"
    while [ "$exit" == "False" ] ;
    do
        echo "
        Digite o nome do usuario"
        read name_user
        exit="True"

        id=$(id $name_user 2> /dev/null | wc -l)  
        if [ $id -eq 1 ]; then
            exit="False"
            echo "
            usuario $name_user já existe!"
        fi
        
    done
    echo "Digite o caminho home do usuario [padrão /home/$name_user]"
    read path_user
    if [ "$path_user" == "" ] ; then
        adduser $name_user
        path_user="/home/$name_user"
    else
        adduser "$name_user" -d "$path_user"
    fi

    echo "123123" | passwd $name_user --stdin


    read -p "Usuario terá acesso ao SUDO [Y/N] " -n 1 -r
    echo    
    if [[ $REPLY =~ ^[Yy]$ ]]
    then
        sudo=1
    else
        sudo=0
    fi

    if [ $sudo == 1 ]
    then

        read -p "Usuario terá acesso ao SU [Y/N] " -n 1 -r
        echo    
        if [[ $REPLY =~ ^[Yy]$ ]]
        then

            echo "$name_user ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers
        fi
        else
            echo "$name_user ALL=(ALL) NOPASSWD: ALL , !/bin/su " >> /etc/sudoers
    fi

    read -p "Usuario terá acesso ao SSH [Y/N] " -n 1 -r
    echo    
    if [[ $REPLY =~ ^[Yy]$ ]]
    then
        echo "AllowUsers $name_user" >> /etc/ssh/sshd_config
        read -p "Usuario usará chaves para acessar o SSH [Y/N] " -n 1 -r
        echo    
            if [[ $REPLY =~ ^[Yy]$ ]]
            then
                ssh-keygen -b 2048 -t rsa -f /tmp/chaves/"$name_user" -q -N ""
                cd $path_user
                mkdir .ssh
                cd .ssh
                cp /tmp/chaves/"$name_user".pub authorized_keys
                chown "$name_user": authorized_keys
                echo "Chave PRIVADA criada em /tmp/chaves/"
            fi

    fi


    read -p "Usuario terá acesso ao GOOGLE-AUTHENTICATOR? [Y/N] " -n 1 -r
    echo    
    if [[ $REPLY =~ ^[Yy]$ ]]
    then
            read -p "
            ################################################
            DEIXE O TERMINAL EM TELA CHEIA PARA VER O QRCODE
            PRECIONE QUALQUER TECLA PARA CONTINUAR
            ################################################ " -n 1
            echo    
            su -c "yes | google-authenticator" $name_user

           
    fi
     



done

echo "

Configs finais do Script

"

sed -i 's/PermitRootLogin yes.*/PermitRootLogin no/' /etc/ssh/sshd_config 2> ~/log/ssh_sed
systemctl restart sshd 2> ~/log/ssh_reboot

cd /usr/local/bin
sed -i 's/inet_protocols = all.*/inet_protocols = ipv4/' /etc/postfix/main.cf
systemctl enable postfix
systemctl start postfix

cd /usr/local/bin
echo "" > notificar-login-via-email.sh
echo '#!/bin/sh
export LANG=pt_BR.UTF-8
if [ $PAM_TYPE != "open_session" ];
then
   exit 0
else
{
  echo -e "Usuário  : $PAM_USER\n"
  echo -e "IP Remoto: $PAM_RHOST\n"
  echo -e "Serviço  : $PAM_SERVICE\n"
  echo -e "TTY      : $PAM_TTY\n"
  echo -e "Data     : $(date)\n"
  echo -e "Servidor : $(uname -a)\n"
} | mail -s "[$PAM_SERVICE] Login efetuado com sucesso: $PAM_USER@$(hostname)" apavanello@live.com 
fi
exit 0
' > notificar-login-via-email.sh
chmod a+x notificar-login-via-email.sh

echo "session optional pam_exec.so /usr/local/bin/notificar-login-via-email.sh" > /etc/pam.d/sshd